import React, { useState } from "react";
import { Routes, Route } from "react-router-dom";
import Header from "../header/Header";
import Footer from "../footer/footer";
import Drawer from "../../globalComponents/drawer/Drawer";
import Table from "../../pages/table/Table";
import Login from "../../pages/login/Login";
import Home from "../../pages/home/Home";
import FilmSeries from "../../pages/filmSeries/FilmSeries";
import Cartoon from "../../pages/cartoon/Cartoon";
import Search from "../../pages/search/Search";
import PrivateComonent from "../privateComponent/Privateomponent";
import clsx from "clsx";
import useStyles from "./Styles";
export default function Layout() {
  const [showDrawer, setShowDrawer] = useState(false);
  function handleDrawer() {
    setShowDrawer(!showDrawer);
  }
  let classes = useStyles();
  return (
    <div className={classes.root}>
      <div
        className={clsx({
          [classes.header]: showDrawer === false,
          [classes.contentDrawerOpen]: showDrawer === true,
        })}
      >
        <Header handleDrawer={handleDrawer} openDrawer={showDrawer} />
      </div>
      <div className={classes.divContentDrawer}>
        <div
          className={clsx({
            [classes.contentDrawerClose]: showDrawer === false,
            [classes.contentDrawerOpen]: showDrawer === true,
          })}
        >
          <Routes>
            <Route exact path="/" element={<Home />} />
            <Route exact path="/home" element={<Home />} />
            <Route exact path="/login" element={<Login />} />
            <Route exact path="/filmSeries" element={<FilmSeries />} />
            <Route exact path="/cartoon" element={<Cartoon />} />
            <Route exact path="/search" element={<Search />} />
            <Route
              exact
              path="/table"
              element={
                <PrivateComonent>
                  <Table />
                </PrivateComonent>
              }
            />
          </Routes>
        </div>
        <div
          className={clsx(classes.drawer, {
            [classes.drawerClose]: showDrawer === false,
            [classes.drawerOpen]: showDrawer === true,
          })}
        >
          <Drawer handleDrawer={handleDrawer} openDrawer={showDrawer} />
        </div>
      </div>
      <Footer />
    </div>
  );
}
