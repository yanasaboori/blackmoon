import React from "react";
import Grid from "@material-ui/core/Grid";
// css
import useStyles from "./Styles";
export default function ShowData({ item }) {
  let classes = useStyles();
  return (
    <Grid container className={classes.root} justifyContent="center">
      <Grid
        item
        container
        xl={7}
        lg={7}
        md={7}
        sm={8}
        xs={8}
        justifyContent="space-between"
      >
        <Grid item xl={5} lg={5} md={5} sm={5} xs={5}>
          <img src={item.srcImg} className={classes.img} />
        </Grid>
        <Grid
          item
          container
          xl={2}
          lg={2}
          md={2}
          sm={3}
          xs={3}
          direction="column"
          alignItems="flex-end"
        >
          <Grid item>
            <h4>{item.nameFilm}</h4>
          </Grid>
          <Grid item>
            {" "}
            <div>
              <span>شماره :{item.id}</span>&nbsp;
              <br />
            </div>
          </Grid>
          <Grid item className={classes.kkk}>
            <hr />
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}
