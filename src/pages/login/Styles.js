import { makeStyles } from "@material-ui/core/styles";
//img

export default makeStyles((theme) => ({
  root: {
    minHeight: "100vh",
    marginTop: "80px",
  },
  containerInto: {
    height: "500px",
    borderBottom: "2px solid #de9f0b",
  },
  imgLogin: {
    width: "200px",
    height: "140px",
  },
  textinputPass: {
    fontWeight: "500",
    position: "relative",
    bottom: "20px",
  },
  input: {
    width: "230px",
    height: "38px",
    border: "none",
    backgroundColor: "lightgrey",
    borderRadius: "10px",
    textAlign: "center",
  },
  buttonLogin: {
    cursor: "pointer",
    width: "93px",
    height: "37px",
    borderRadius: "7px",
    backgroundColor: "lightgrey",
    border: "none",
  },

  hidden: {
    display: "none",
    color: "blue",
  },
  error: {
    color: "red",
    display: "block",
  },
}));
