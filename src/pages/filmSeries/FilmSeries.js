import React, { useState } from "react";

// componnets
import CarouselCover from "../../globalComponents/carouselComponent/carouselCover/CarouselCover";
import Slider from "../.././slider/Slider";
import CarouselLarge from "../../globalComponents/carouselLarge/CarouselLarge";
import CarouselSample from "../../globalComponents/carouselComponent/carouselsample/CrouselSmple";

// object
import { cro1, cro2, pictureC } from "../../globalObjects/gloalObject";

// css
import useStyles from "./Styles";
export default function FilmSeries() {
  let classes = useStyles();
  return (
    <div className={classes.root}>
      <Slider />
      <CarouselSample
        showAll="تخیلی"
        newFilm="جدیدترین فیلم ها"
        arrayCarousel={cro1}
      />
      <CarouselLarge arrayObject={pictureC} />
      <CarouselCover
        showAll=" پربازدیدترین  "
        newFilm="جدیدترین فیلم ها"
        arrayCarousel={cro2}
      />
      <CarouselLarge arrayObject={pictureC} />
      <CarouselSample
        showAll=" کمدی"
        newFilm="جدیدترین فیلم ها"
        arrayCarousel={cro1}
      />
    </div>
  );
}
