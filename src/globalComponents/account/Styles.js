import { makeStyles } from "@material-ui/core/styles";

export default makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    width: "240px",
    height: "350px",
    "& h3": {
      borderBottom: "2px groove    #de9f0b",
      height: "100%",
      margin: "0px",
    },
  },
  heightTag: {
    height: "45px",
    textAlign: "center",
  },
}));
