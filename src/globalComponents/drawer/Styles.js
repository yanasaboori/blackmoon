import { makeStyles } from "@material-ui/core/styles";

export default makeStyles((theme) => ({
  root: {
    width: "100%",
    height: "282px",
  },

  link: {
    textDecoration: "none",
    textAlign: "center",
    marginLeft: "3px",
    color: "black",
    display: "flex",
    alignItems: "center",
    [theme.breakpoints.down("xs")]: {
      fontSize: "11px",
    },
  },
  contlists: {
    marginTop: "10px",
    height: "282px",
  },
}));
