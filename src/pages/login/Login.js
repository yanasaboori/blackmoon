import React, { useState } from "react";
import { Grid } from "@material-ui/core";
import useStyle from "./Styles";
import { Classnames } from "react-alice-carousel";
import classnames from "classnames";
import { useNavigate } from "react-router-dom";
import { loginUser, useUserDispatch } from "../../context/useContext";
import LoginPic from "../../images/Login.jpg";

export default function Login() {
  // global
  var userDispatch = useUserDispatch();
  let navigate = useNavigate();
  const [number, setNumber] = useState(100);
  const [showError, setShowError] = useState(false);

  function handelInput(e) {
    setNumber(e.target.value);
  }
  function handleLogin() {
    if (number != 100) {
      setShowError(true);
    } else if (number == 100) {
      setShowError(false);
      loginUser(userDispatch, number, navigate);
    }
  }

  let classes = useStyle();
  return (
    <Grid container justifyContent="center" className={classes.root}>
      <Grid
        item
        container
        xl={6}
        lg={6}
        md={6}
        sm={11}
        xs={11}
        direction="column"
        alignItems="center"
        justifyContent="space-between"
        className={classes.containerInto}
      >
        <img src={LoginPic} className={classes.imgLogin} alt="" />
        <h5 className={classes.textinputPass}>کد ورود را وارد نمائید </h5>
        <input
          type="text"
          id="number"
          name="code"
          value={number}
          onChange={handelInput}
          className={classes.input}
        />

        <h5
          className={classnames(classes.hidden, {
            [classes.error]: showError === true,
          })}
        >
          لطفا برای ورود عدد 100 را وارد کنید{" "}
        </h5>
        <button className={classes.buttonLogin} onClick={handleLogin}>
          ورود / ثبت نام
        </button>

        <h6>قوانین استفاده از سایت را میپذیرم</h6>
      </Grid>
    </Grid>
  );
}
/////////////////////////////
