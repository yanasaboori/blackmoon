import React from "react";
import sefidbrfi from "../../images/sefidbarfi.jpg";
import { newcoodak, pictureCoodak } from "../../globalObjects/gloalObject";

// componnets
import ComponentImg from "../../globalComponents/componentImg/ComponentImg";
import CarouselCover from "../../globalComponents/carouselComponent/carouselCover/CarouselCover";
import CarouselLarge from "../../globalComponents/carouselLarge/CarouselLarge";
// object

// css
import useStyles from "./Styles";
export default function Cartoon() {
  let classes = useStyles();
  return (
    <div className={classes.root}>
      <ComponentImg src={sefidbrfi} />
      <CarouselCover
        showAll=" کمدی"
        newFilm="جدیدترین فیلم ها"
        arrayCarousel={newcoodak}
      />
      <CarouselLarge arrayObject={pictureCoodak} />
    </div>
  );
}
