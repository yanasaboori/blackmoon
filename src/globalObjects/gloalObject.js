//imgCoodak
import sefidBarfy from "../images/sefidbarfi.jpg";
import hana from "../images/hana.jpg";
import sindrela from "../images/sindrela.jpg";
import pin from "../images/pinocchiojpg.jpg";
import CoverSlid from "../globalComponents/carouselComponent/coverSlide/CoverSlide";
//imgcarousel
import fiilSHah from "../images/fiilshah.jpg";
import gharnesheenan from "../images/gharnesheenan.jpg";
import cat from "../images/cat.jpg";
import shereck from "../images/sherek.jpg";
import sleepingBeayty from "../images/sleepingBeauty.jpg";
import animalse from "../images/animals.jpg";
import delbar from "../images/delbar.jpg";
//imgFilm
import zakhm from "../images/zakhm.jpg";
import nowroze from "../images/nowrooze.jpg";
import dellKHon from "../images/dellkhon.jpg";
import nazar from "../images/nazarjpg.jpg";
import rozi from "../images/rozi.jpg";
import sant0ori from "../images/santoori.jpg";
import shadravan from "../images/shadravan.jpg";
import tacKhal from "../images/tackhal.jpg";
import mahi from "../images/mahi.jpg";
import haftonim from "../images/haftonim.jpg";
import zendani from "../images/zendani.jpg";
//cro2
import joonon from "../images/jonoon.jpg";
import mordad from "../images/Mordad-Poster.jpg";
import mankan from "../images/mankan.jpg";
import bootax from "../images/bootax.jpg";

export let newFilmCarousel = [
  {
    id: 1,
    srcImg: zakhm,
    nameFilm: "زخم",
    height: "230px",
    width: "94%",
    cover: <CoverSlid />,
  },
  {
    id: 2,
    srcImg: nowroze,
    nameFilm: "نوروز",
    cover: <CoverSlid />,
    height: "230px",
    width: "94%",
  },
  {
    id: 3,
    srcImg: tacKhal,
    nameFilm: " تک خال",
    height: "230px",
    width: "94%",
    cover: <CoverSlid />,
  },
];
// StartCarousel
export const newcoodak = [
  {
    id: 1,
    srcImg: fiilSHah,
    nameFilm: "  فیل شاه",
    height: "230px",
    width: "94%",
    cover: <CoverSlid />,
  },
  {
    id: 2,
    srcImg: gharnesheenan,
    nameFilm: "غارنشینان",
    height: "230px",
    width: "94%",
    cover: <CoverSlid />,
  },
  {
    id: 3,
    srcImg: cat,
    nameFilm: " گربه چکمه پوش",
    height: "230px",
    width: "94%",
    cover: <CoverSlid />,
  },

  {
    id: 5,
    srcImg: shereck,
    nameFilm: "شرک",
    height: "230px",
    width: "94%",
    cover: <CoverSlid />,
  },
  {
    id: 6,
    srcImg: sleepingBeayty,
    nameFilm: "سفیدبرفی",
    height: "230px",
    width: "94%",
    cover: <CoverSlid />,
  },
  {
    id: 7,
    srcImg: delbar,
    nameFilm: "دیو و دلبر",
    width: "94%",
    height: "230px",
    cover: <CoverSlid />,
  },
  {
    id: 8,
    srcImg: animalse,
    height: "230px",
    width: "94%",
    nameFilm: "زندگی مخفی حیوانات",
    cover: <CoverSlid />,
  },
];
export const cro2 = [
  {
    id: 1,
    srcImg: joonon,
    nameFilm: "جنون ",
    height: "230px",
    width: "94%",
    cover: <CoverSlid name=" جنون " id={1} />,
  },
  {
    id: 2,
    srcImg: mordad,
    nameFilm: "مرداد",
    height: "230px",
    width: "94%",
    cover: <CoverSlid name=" مرداد " id={2} />,
  },
  {
    id: 3,
    srcImg: bootax,
    nameFilm: " بوتاکس",
    height: "230px",
    width: "94%",
    cover: <CoverSlid name=" بوتاکس" id={3} />,
  },
  {
    id: 4,
    srcImg: rozi,
    nameFilm: "روزی",
    height: "230px",
    width: "94%",
    cover: <CoverSlid name="روزی" id={4} />,
  },
  {
    id: 5,
    srcImg: mankan,
    nameFilm: "مانکن",
    height: "230px",
    width: "94%",
    cover: <CoverSlid name="مانکن" id={5} />,
  },
];
export const cro1 = [
  {
    id: 1,
    srcImg: dellKHon,
    nameFilm: "دل خون",
  },
  {
    id: 2,
    srcImg: shadravan,
    nameFilm: "شادروان",
  },
  {
    id: 3,
    srcImg: sant0ori,
    nameFilm: " سنتوری",
  },
  {
    id: 4,
    srcImg: rozi,
    nameFilm: "روزی",
  },
  {
    id: 5,
    srcImg: nazar,
    nameFilm: "زیرنظر",
  },
  {
    id: 6,
    srcImg: haftonim,
    nameFilm: "هفت و نیم",
  },
  {
    id: 7,
    srcImg: zendani,
    nameFilm: "زندانی",
  },
  {
    id: 8,
    srcImg: mahi,
    nameFilm: "  ماهی و گربه",
  },
];
export const pictureC = [
  {
    id: 1,
    srcImg: mahi,
    nameFilm: "  ماهی و گربه",
    height: "230px",
    width: "94%",
  },
  {
    id: 2,
    srcImg: zendani,
    nameFilm: "زندانی",
    height: "230px",
    width: "94%",
  },
  {
    id: 3,
    srcImg: haftonim,
    nameFilm: "هفت و نیم",
    height: "230px",
    width: "94%",
  },
  {
    id: 4,

    srcImg: sant0ori,
    nameFilm: " سنتوری",
    height: "230px",
    width: "94%",
  },
];
export const pictureCoodak = [
  {
    id: 1,
    srcImg: sefidBarfy,
    nameFilm: " سفید برفی و هفت کوتوله",
    height: "230px",
    width: "94%",
  },
  {
    id: 2,
    srcImg: hana,
    nameFilm: "حنا",
    height: "230px",
    width: "94%",
  },
  {
    id: 3,
    srcImg: pin,
    nameFilm: "پینوکیو",
    height: "230px",
    width: "94%",
  },
  {
    id: 4,
    srcImg: sindrela,
    nameFilm: "سیندرلا",
    height: "230px",
    width: "94%",
  },
];
