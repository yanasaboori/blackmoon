import React, { useState } from "react";
import AliceCarousel from "react-alice-carousel";
import Grid from "@material-ui/core/Grid";
import ShowData from "../showData/ShowData";
// css
import "react-alice-carousel/lib/alice-carousel.css";
import useStyles from "./Styles";
// import "./v.css";

export default function CarouselSmple({ showAll, newFilm, arrayCarousel }) {
  const [item, setItem] = useState(null);
  const [showData, setShowData] = useState(false);
  function handleShowInfo(e) {
    let id = Number(e.target.id);
    if (id !== window.setIdItem) {
      setShowData(true);
      setItem(arrayCarousel.find((element) => element.id === id));
      window.setIdItem = id;
    } else if (id === window.setIdItem) {
      setShowData(!showData);
      setItem(arrayCarousel.find((element) => element.id === id));
    }

    // let id = Number(event.target.id);
    // setItem(arrayCarousel.find((element) => element.id === id));
  }
  const responsive = {
    0: {
      items: 3,
    },

    600: {
      items: 5,
    },
    1024: {
      items: 7,
    },
  };

  let classes = useStyles();

  return (
    <Grid container justifyContent="center">
      <div className={classes.divRow1}>
        <h4>{showAll}</h4> <h4>{newFilm}</h4>
      </div>
      <Grid item container lg={11} xl={11} md={11} className={classes.root2}>
        <AliceCarousel responsive={responsive}>
          {arrayCarousel.map((item) => {
            return (
              <div className={classes.divCover}>
                <img
                  src={item.srcImg}
                  className={classes.imgs}
                  id={item.id}
                  onClick={handleShowInfo}
                />
                <h5 className={classes.nameFilm}>{item.nameFilm}</h5>
              </div>
            );
          })}
        </AliceCarousel>
      </Grid>
      {showData === true ? <ShowData item={item} /> : null}
    </Grid>
  );
}
