import React from "react";
import Grid from "@material-ui/core/Grid";
import "react-alice-carousel/lib/alice-carousel.css";
import AliceCarousel from "react-alice-carousel";
import "react-alice-carousel/lib/alice-carousel.css";
// css
import useStyles from "./Styles";
export default function CarouselLarge({ arrayObject }) {
  const responsive = {
    0: {
      items: 2,
    },
    600: {
      items: 3,
    },
    1024: {
      items: 4,
    },
  };

  let classes = useStyles();
  return (
    <Grid container>
      <AliceCarousel
        responsive={responsive}
        fadeOutAnimation
        infinite
        mouseDragEnabled
        autoPlay
        autoPlayDirection="rtl"
        autoPlayInterval={2000}
      >
        {arrayObject.map((item) => {
          return (
            <img src={item.srcImg} className={classes.img} key={item.id} />
          );
        })}
      </AliceCarousel>
    </Grid>
  );
}
