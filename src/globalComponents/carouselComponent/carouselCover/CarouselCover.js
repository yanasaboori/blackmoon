import React, { useState } from "react";
import AliceCarousel from "react-alice-carousel";
import Grid from "@material-ui/core/Grid";
import ShowData from "../showData/ShowData";
// css
import "react-alice-carousel/lib/alice-carousel.css";
import useStyles from "./Styles";
import "./styles.css";
import CoverSlid from "../coverSlide/CoverSlide";

export default function CarouselCover({ showAll, newFilm, arrayCarousel }) {
  const [item, setItem] = useState(null);
  const [showData, setShowData] = useState(false);

  const responsive = {
    0: {
      items: 3,
    },

    600: {
      items: 5,
    },
    1024: {
      items: 7,
    },
  };

  let classes = useStyles();

  return (
    <Grid container justifyContent="center">
      <div className={classes.divRow1}>
        <h4>{showAll}</h4> <h4>{newFilm}</h4>
      </div>
      <Grid item container lg={11} xl={11} md={11} className={classes.root2}>
        <AliceCarousel responsive={responsive}>
          {arrayCarousel.map((item) => {
            return (
              <div className={classes.divCover} key={item.id}>
                <div>
                  <CoverSlid
                    itemId={item.id}
                    nameFilm={item?.nameFilm}
                    setItem={setItem}
                    showData={showData}
                    setShowData={setShowData}
                    arrayCarousel={arrayCarousel}
                  />
                </div>
                <img
                  src={item.srcImg}
                  widht={item.widht}
                  height={item.height}
                  className={classes.imgs}
                />
              </div>
            );
          })}
        </AliceCarousel>
      </Grid>
      {showData === true ? <ShowData item={item} /> : null}
    </Grid>
  );
}
