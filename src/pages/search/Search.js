import React, { useState } from "react";
import Grid from "@mui/material/Grid";
import PropTypes from "prop-types";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Typography from "@mui/material/Typography";
import AppBar from "@material-ui/core/AppBar";
import Box from "@mui/material/Box";
import { BsCheckLg } from "react-icons/bs";
import { MdOutlineSettingsBackupRestore } from "react-icons/md";
import { MdSearch } from "react-icons/md";
import { FaChild } from "react-icons/fa";
import { GiFilmSpool } from "react-icons/gi";

import { Fab } from "@mui/material";
//css
import useStyles from "./Styles";
//object
import { cro2, newcoodak } from "../../globalObjects/gloalObject";
function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 5 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

export default function BasicTabs() {
  const [value, setValue] = useState(0);
  const [searchValue, setSearchValue] = useState("");
  const [film, setFilm] = useState(cro2);
  const [coodak, setCoodak] = useState(newcoodak);
  const [showBBack, setShowBack] = useState(false);
  const handleOnChange = (event, newValue) => {
    setValue(newValue);
  };
  const handlesearchValue = (event) => {
    setSearchValue(event.target.value);
  };
  const handelFilter = () => {
    setFilm(cro2.filter((i) => i.nameFilm === searchValue));
    setCoodak(newcoodak.filter((i) => i.nameFilm === searchValue));
    setShowBack(true);
  };
  const handelBack = () => {
    setFilm(cro2);
    setCoodak(newcoodak);
    setShowBack(false);
  };

  let classes = useStyles();
  return (
    <Grid container justifyContent="center" className={classes.root}>
      <Grid
        item
        container
        xl={8}
        lg={8}
        md={8}
        sm={8}
        xs={8}
        justifyContent="space-around"
        className={classes.containerInputApplay}
      >
        <Grid
          item
          conteiner
          xl={2}
          lg={2}
          md={2}
          sm={2}
          xs={2}
          className={classes.containericons}
        >
          <Grid item>
            {" "}
            {showBBack ? (
              <Fab size="small" onClick={handelBack}>
                <MdOutlineSettingsBackupRestore size="20px" color="black" />
              </Fab>
            ) : null}
          </Grid>
          <Grid item>
            {" "}
            <Fab size="small" onClick={handelFilter}>
              <BsCheckLg size="20px" color="black" onClick={handelFilter} />
            </Fab>
          </Grid>
        </Grid>
        <Grid item xl={5} lg={5} md={5} sm={5} xs={5}>
          {" "}
          <div>
            <input
              className={classes.inputValue}
              value={searchValue}
              onChange={handlesearchValue}
              placeholder="..جستجو کنید"
            />
            <MdSearch
              className={classes.iconSearch}
              size="25px"
              color="#b3b5b4"
            />
          </div>{" "}
        </Grid>
      </Grid>

      <div className={classes.divTabs}>
        <AppBar position="static" style={{ backgroundColor: "black" }}>
          <Tabs
            value={value}
            onChange={handleOnChange}
            aria-label="basic tabs example"
            style={{ color: "white" }}
            textColor="inherit"
            variant="fullWidth"
          >
            <Tab
              label="فیلم و سریال"
              icon={<GiFilmSpool size="20px" />}
              {...a11yProps(0)}
            />
            <Tab
              label=" کودک"
              icon={<FaChild size="20px" />}
              {...a11yProps(1)}
            />
          </Tabs>
        </AppBar>
        <TabPanel value={value} index={0}>
          <Grid container justifyContent="center">
            {film.length === 0 ? (
              <h2>متاسفانه موردی یافت نشد</h2>
            ) : (
              film.map((item) => {
                return (
                  <div>
                    <Grid item>
                      <img
                        src={item.srcImg}
                        width={item.width}
                        height={item.height}
                        className={classes.img}
                        alt=""
                      />
                    </Grid>
                    <h4 style={{ textAlign: "center" }}>{item.nameFilm}</h4>
                  </div>
                );
              })
            )}
          </Grid>
        </TabPanel>
        <TabPanel value={value} index={1}>
          <Grid container justifyContent="center">
            {coodak.length === 0 ? (
              <h2>متاسفانه موردی یافت نشد</h2>
            ) : (
              coodak.map((item) => {
                return (
                  <div>
                    <Grid item>
                      <img
                        src={item.srcImg}
                        width={item.width}
                        height={item.height}
                        className={classes.img}
                        alt=""
                      />
                    </Grid>
                    <h4 className={classes.textname}>{item.nameFilm}</h4>
                  </div>
                );
              })
            )}
          </Grid>
        </TabPanel>
      </div>
    </Grid>
  );
}
