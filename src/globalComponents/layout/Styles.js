import { makeStyles } from "@material-ui/core/styles";
export default makeStyles((theme) => ({
  root: {
    width: "100%",
    height: "100vh",
  },
  divContentDrawer: {
    display: "flex",
  },
  header: {
    width: "100%",
  },
  drawer: {
    height: "100%",
    position: "fixed",
    top: 0,
    right: 0,
    marginTop: "65px",
    backgroundColor: "rgba(255,255,255,0.6)",
  },
  drawerOpen: {
    borderRadius: "20px 0 0 20px ",
    width: "14%",
    backgroundColor: "rgba(255,255,255,0.4)",
    [theme.breakpoints.only("md")]: {
      width: "18%",
    },
    [theme.breakpoints.only("sm")]: {
      width: "23%",
    },
    [theme.breakpoints.only("xs")]: {
      width: "30%",
    },
  },
  drawerClose: {
    borderRadius: "20px 0 0 20px",
    height: "100%",
    width: "3%",
    [theme.breakpoints.only("sm")]: {
      width: "6%",
    },
    [theme.breakpoints.down("xs")]: {
      width: "10%",
    },
  },
  contentDrawerClose: {
    width: "97%",
    [theme.breakpoints.only("sm")]: {
      width: "94%",
    },
    [theme.breakpoints.down("xs")]: {
      width: "90%",
    },
    height: "100%",
    flexGrow: 1,
  },
  contentDrawerOpen: {
    width: "86%",
    [theme.breakpoints.only("md")]: {
      width: "82%",
    },
    [theme.breakpoints.only("sm")]: {
      width: "77%",
    },
    [theme.breakpoints.down("xs")]: {
      width: "70%",
    },
  },
}));
