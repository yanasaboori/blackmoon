import { makeStyles } from "@material-ui/core/styles";

export default makeStyles((theme) => ({
  root: {
    color: "white",
    borderRadius: "0 0  25px 25px",
    height: "60px",
    alignItems: "center",
    backgroundColor: "rgba(255,255,255,0.5)",
    width: "100%",
    fontFamily: "MaktabRita-Regular.woff2",
    [theme.breakpoints.down("xs")]: {
      height: "45px",
    },
    position: "fixed",
    top: 0,
    zIndex: 2000,
  },
  root2: {
    marginLeft: "7px",
    marginRight: "7px",
  },
  socialLin: {
    color: "#de9f0b",
    textDecoration: "none",
    "&:active": {
      color: "golden",
    },
  },
  login: {
    border: "none",
    cursor: "pointer",
    width: "100px",
    height: "40px",
    borderRadius: "17px",
    [theme.breakpoints.down("xs")]: {
      width: "60px",
      height: "30px",
    },
  },
  iconLogOut: {
    [theme.breakpoints.down("xs")]: {
      width: "23px",
    },
  },
  iconLogin: {
    [theme.breakpoints.down("xs")]: {
      width: "23px",
    },
  },
  iconSearch: {
    [theme.breakpoints.down("xs")]: {
      width: "23px",
    },
  },
  logo: {
    width: "45px",
    height: "45px",
  },
}));
