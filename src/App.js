import "./App.css";
import React from "react";
// import Components
import Layout from "./globalComponents/layout/Layout";
import Pupup from "./pupub/Pupub";

// context

function App() {
  return <Layout />;
}

export default App;
