import { makeStyles } from "@material-ui/styles";

export default makeStyles((theme) => ({
  root: {
    position: "absolute",
    backgroundColor: "rgba(100,100,100,0.8)",
    height: "230px",
    width: "94%",
    zIndex: 3000,
    borderRadius: "10px",
    opacity: "0",
    "&:hover": {
      opacity: "1",
    },
  },

  containerinfo: {
    height: "95%",
    width: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-around",
    alignItems: "flex-end",
  },
}));
