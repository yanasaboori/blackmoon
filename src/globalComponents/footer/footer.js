import React from "react";
import Grid from "@material-ui/core/Grid";
import { Link } from "react-router-dom";
import { AiOutlineInstagram } from "react-icons/ai";
import { RiTelegramLine } from "react-icons/ri";
import { AiOutlineWhatsApp } from "react-icons/ai";
import { AiOutlineSkype } from "react-icons/ai";
import UseStyle from "./Styles";

export default function Footer() {
  let classes = UseStyle();
  return (
    <Grid container justifyContent="center" className={classes.root}>
      <Grid container xl={11} lg={11} md={11} sm={11} xs={11}>
        <Grid
          item
          container
          xl={2}
          lg={2}
          md={2}
          sm={4}
          xs={4}
          justifyContent="space-between"
          alignItems="flex-end"
          className={classes.containerIcons}
        >
          <Grid item>
            <AiOutlineSkype size="22px" />
          </Grid>
          <Grid item>
            <AiOutlineInstagram size="22px" />
          </Grid>
          <Grid item>
            <RiTelegramLine size="22px" />
          </Grid>
          <Grid item>
            <AiOutlineWhatsApp size="22px" />
          </Grid>
        </Grid>
        <Grid
          item
          container
          xl={6}
          lg={6}
          md={6}
          sm={4}
          xs={4}
          direction="column"
          alignItems="flex-end"
          className={classes.linkFooter}
        >
          <Grid item>
            <Link className={classes.styleLink} to="/">
              خرید اشتراک
            </Link>
          </Grid>
          <br />
          <Grid item>
            <Link className={classes.styleLink} to="/">
              اپلیکیشن ها
            </Link>
          </Grid>
        </Grid>
        <Grid
          className={classes.linkFooter}
          item
          container
          xl={4}
          lg={4}
          md={4}
          sm={4}
          xs={4}
          direction="column"
          alignItems="flex-end"
        >
          <Grid item>
            <Link className={classes.styleLink} to="/">
              درباره ی ما
            </Link>
          </Grid>
          <br />
          <Grid item>
            <Link className={classes.styleLink} to="/">
              تماس با ما
            </Link>
          </Grid>
          <br />
          <Grid item>
            <Link className={classes.styleLink} to="/">
              قوانین و مقررات
            </Link>
          </Grid>
          <br />
          <Grid item>
            <Link className={classes.styleLink} to="/">
              سوالات متداول
            </Link>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}
