import { makeStyles } from "@material-ui/styles";

export default makeStyles((theme) => ({
  root2: {
    position: "relative",
    width: "100%",
  },
  imgs: {
    position: "relative",
    borderRadius: "10px",
    height: "230px",
    width: "94%",
    "&:hover": {
      height: "200px",
      width: "89%",
    },
  },

  divRow1: {
    width: "97%",
    display: "flex",
    justifyContent: "space-between",
  },
  nameFilm: {
    textAlign: "center",
  },
}));
