import React, { useEffect } from "react";

// css
import useStyles from "./styles";

export default function CoverSlid({
  nameFilm,
  itemId,
  setItem,
  setShowData,
  showData,
  arrayCarousel,
}) {
  function handelShowInfo(e) {
    if (e.target.lastElementChild.textContent !== window.setIdItem) {
      setShowData(true);
      setItem(arrayCarousel.find((element) => element.id === itemId));
      window.setIdItem = e.target.lastElementChild.textContent;
    } else if (e.target.lastElementChild.textContent === window.setIdItem) {
      setShowData(!showData);
      setItem(arrayCarousel.find((element) => element.id === itemId));
    }
  }

  let classes = useStyles();
  return (
    <div className={classes.root} onClick={handelShowInfo}>
      <div className={classes.containerinfo}>
        <h3>{nameFilm}</h3>
        <h5>{itemId}</h5>
      </div>
    </div>
  );
}
